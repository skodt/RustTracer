
use vector::Vector3;
use color::Color;
use geometry::*;
use geometry::sphere::*;
use std::f64;

pub struct Vertex {
    pub pt: Vector3,
    pub color: Option<Color>,
}

pub struct Face {
    pub a: usize,
    pub b: usize,
    pub c: usize,
    pub color: Option<Color>,
}

pub struct Mesh {
    pub vertices: Vec<Vertex>,
    pub faces: Vec<Face>,
    bounding_sphere: Option<Sphere>,
}

macro_rules! face {
    ($a:expr, $b:expr, $c:expr) => (Face { a: $a, b: $b, c: $c, color: None })
}

impl Mesh {
    pub fn new() -> Mesh {
        Mesh {
            vertices: Vec::new(),
            faces: Vec::new(),
            bounding_sphere: None,
        }
    }

    pub fn offset(&mut self, off: Vector3) -> () {
        for i in 0 .. self.vertices.len() {
            self.vertices[i].pt = self.vertices[i].pt + off
        }
        if let Some (ref mut s) = self.bounding_sphere {
            s.center = s.center + off
        }
    }

    /// Finalize the construction of a mesh.
    /// Here we compute an approximation of the smallest bounding sphere
    /// for faster instersection checks.
    pub fn finalize(&mut self) -> () {
        if !self.bounding_sphere.is_none() {
            return
        }
        // The algorithm for the bounding sphere is Ritter's
        // method for an efficient bounding sphere.

        fn distance2(mesh: &Mesh, x: usize, y: usize) -> f64 {
            (mesh.pt(x) - mesh.pt(y)).norm()
        }

        let mut minx = 0;
        let mut maxx = 0;
        let mut miny = 0;
        let mut maxy = 0;
        let mut minz = 0;
        let mut maxz = 0;

        for x in 1..self.vertices.len() {
            let v = self.pt(x);
            if v.x < self.pt(minx).x {
                minx = x;
            } else if v.x > self.pt(maxx).x {
                maxx = x;
            }
            if v.y < self.pt(miny).y {
                miny = x;
            } else if v.y > self.pt(maxy).y {
                maxy = x;
            }
            if v.z < self.pt(minz).z {
                minz = x;
            } else if v.z > self.pt(maxz).z {
                maxz = x;
            }
        }

        // Keep the pair min, max that are most distanced.
        let dx = distance2(self, minx, maxx);
        let dy = distance2(self, miny, maxy);
        let dz = distance2(self, minz, maxz);
        let pair =
            if dx >= dy && dx >= dz {
                (minx, maxx)
            } else if dy >= dx && dy >= dz {
                (miny, maxy)
            } else {
                (minz, maxz)
            };

        let mut radius2 = distance2(self, pair.0, pair.1) * 0.25;
        let mut radius = radius2.sqrt();
        let mut center = (self.pt(pair.0) + self.pt(pair.1)) * 0.5;

        for x in 0..self.vertices.len() {
            let d2 = (self.pt(x) - center).norm();
            if d2 > radius2 {
                let d = d2.sqrt();
                radius = (radius + d) * 0.5;
                radius2 = radius * radius;
                center = (center * radius + self.pt(x) * (radius - d)) / d;
            }
        }

        self.bounding_sphere = Some (Sphere { center: center, radius: radius, });
    }

    fn pt(&self, index: usize) -> Vector3 {
        self.vertices[index].pt
    }
}

impl Face {
    fn intersect(&self, ray: &Line, mesh: &Mesh) -> Option<(f64,Vector3)> {
        // Get a normal vector of the plane.
        let (a,b,c) = (self.a,self.b,self.c);

        let normal = (mesh.pt(b)-mesh.pt(a)).cross(&(mesh.pt(c)-mesh.pt(a)));
        let normal = if normal.dot(&ray.direction) <= 0. {-normal} else {normal};

        if normal.dot(&ray.direction) < 1e-6 {
            // The ray and the plane are parallel
            None
        } else {
            // The ray is parametrized as o + t.d.
            let t = normal.dot(&(mesh.pt(a) - ray.origin)) / normal.dot(&ray.direction);
            if t < 0. {
                None
            } else {
                let p_i = ray.origin + t * ray.direction;

                // Test whether the intersection is in the triangle:
                if (mesh.pt(a)-p_i).cross(&(mesh.pt(b)-p_i)).dot(&(mesh.pt(b)-p_i).cross(&(mesh.pt(c)-p_i))) >= 0.
                && (mesh.pt(b)-p_i).cross(&(mesh.pt(c)-p_i)).dot(&(mesh.pt(c)-p_i).cross(&(mesh.pt(a)-p_i))) >= 0. {
                    Some((t,normal))
                } else {
                    None
                }
            }
        }
    }
}

impl Geometry for Mesh {
    fn intersect(&self, ray: &Line) -> Option<(f64, Vector3)> {
        // Check against the bounding sphere first.
        match &self.bounding_sphere {
            Some (s) if s.intersect(ray).is_none() => return None,
            _ => (),
        }

        let mut distance = f64::MAX;
        let mut normal = Vector3::zero();
        for i in 0..self.faces.len() {
            if let Some((d,n)) = self.faces[i].intersect(&ray,&self) {
                if d < distance {
                    distance = d;
                    normal = n;
                }
            }
        }
        if distance == f64::MAX { None } else { Some((distance, normal)) }
    }

    fn normal(&self, _: &Vector3) -> Vector3 {
        Vector3::zero()
    }
}
