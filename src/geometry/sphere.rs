
use geometry::*;

pub struct Sphere {
    pub center: Vector3,
    pub radius: f64,
}

impl Geometry for Sphere {
    fn intersect(&self, ray: &Line) -> Option<(f64,Vector3)> {
        // Build a line segment between the ray origin and the center of the
        // sphere
        let l: Vector3 = self.center - ray.origin;
        // Find the length of the adjacent side
        let adj_length = l.dot(&ray.direction);
        // Square of the length of the opposite side
        let d2 = l.dot(&l) - adj_length * adj_length;
        let radius2 = self.radius * self.radius;

        if d2 > radius2 {
            None
        } else {
            let thickness = (radius2 - d2).sqrt();
            let t0 = adj_length - thickness;
            let t1 = adj_length + thickness;

            if t0 < 0.0 && t1 < 0.0 {
                None
            } else if t0 < 0.0 {
                let n = self.normal(&(ray.origin + t1 * ray.direction));
                Some((t1, n))
            } else {
                let n = self.normal(&(ray.origin + t0 * ray.direction));
                Some((t0, n))
            }
        }
    }

    fn normal(&self, hit_point: &Vector3) -> Vector3 {
        (*hit_point - self.center).normalize()
    }
}
