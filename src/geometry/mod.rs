
use vector::*;

pub mod sphere;
pub mod plane;
pub mod mesh;

pub struct Line {
    pub origin: Vector3,
    pub direction: Vector3,
}

pub trait Geometry : Sync {
    fn intersect(&self, ray: &Line) -> Option<(f64,Vector3)>;
    fn normal(&self, hit_point: &Vector3) -> Vector3;
}
