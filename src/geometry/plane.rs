use geometry::*;

pub struct Plane {
    pub origin: Vector3,
    pub normal: Vector3,
}

impl Geometry for Plane {
    fn intersect(&self, ray: &Line) -> Option<(f64,Vector3)> {
        let normal = self.normal(&Vector3::zero());
        let denom = normal.dot(&ray.direction);
        if denom > 1e-6 {
            let v = self.origin - ray.origin;
            let distance = v.dot(&normal) / denom;
            if distance >= 0.0 {
                Some((distance,normal))
            } else {
                None
            }
        } else {
            None
        }
    }

    fn normal(&self, _: &Vector3) -> Vector3 {
        -self.normal
    }
}
