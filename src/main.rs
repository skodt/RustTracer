mod scene;
mod sample;
mod renderer;
mod geometry;
mod io;

#[macro_use]
mod vector;

#[macro_use]
mod color;

extern crate rand;
extern crate rayon;

use renderer::{render,render_par};
use scene::*;
use color::Color;
use vector::Vector3;

use geometry::mesh::*;

use io::off::import;

use std::fs::File;
use std::io::Write;

fn main() {
    /*let v_a = Vertex { pt: point!(-4.,-2.,-4.), color: None };
    let v_b = Vertex { pt: point!(-0.,2.,-4.), color: None };
    let v_c = Vertex { pt: point!(4.,-2.,-4.), color: None };
    
    let triangle = Face { a: &v_a, b: &v_b, c: &v_c, color: None };

    let elements = vec![
        Element {
            geometry: &triangle,
            material: Material { 
                color: color::RED,
                albedo: 1.0,
                typ: SurfaceType::Diffuse,
            }
        },
    ];*/

    let mut mesh = Mesh::new();
    let file_name = "objects/teapot.off";
    let mut file = File::open(file_name).unwrap();
    
    let res = import(&mut file,&mut mesh);
    if res == None {
        eprint!("Failed to import mesh from file {}", file_name);
        return;
    }
    
    mesh.offset(vector!(0.0,0.0,0.0));

    let elements = vec![
        Element {
            geometry: &mesh,
            material: Material {
                color: color!(0.6, 0.6, 0.6),
                albedo: 1.0,
                typ: SurfaceType::Diffuse,
            }
        },
    ];

    let scene = Scene {
        width: 800,
        height: 600,
        fov: 95.0,
        elements: elements,
        lights: vec![
            Light {
                typ: LightType::Spherical(point!(0.0, 15.0, -5.0)),
                color: color::WHITE,
                intensity: 10000.0,
            },

        ],
        background_color: color!(0.2, 0.2, 0.6),
        bias: 1e-13,
        max_rebound: 3,
        nb_samples: 4,
        nb_diffuse_rays: 4,
    };

    let image_buf = &render_par(&scene);

    let mut file = File::create("img.ppm").unwrap();
    let header = format!("P6 {} {} 255\n", scene.width, scene.height);
    file.write(header.as_bytes());
    file.write(image_buf);
}
