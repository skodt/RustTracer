use scene::*;
use vector::*;
use geometry::Line;
use color::*;
use std::f64::consts::PI;
use std::f64;
use std::io::stdout;
use std::io::Write;
use std::sync::Mutex;
use rand::prelude::*;
use rayon::prelude::*;

type Ray = Line;

pub fn render(scene: &Scene) -> Vec<u8> {
    let mut image : Vec<u8> = vec![0; (scene.height * scene.width * 3) as usize];

    let aspect_ratio = (scene.width as f64) / (scene.height as f64);
    let fov_adjustment = (scene.fov.to_radians() / 2.).tan();

    for x in 0..scene.width {

        print!("\r{:02}%", (x as f32/scene.width as f32 * 100.) as u32);
        stdout().flush();

        for y in 0..scene.height {
            let mut color = BLACK;
            // Get samples of colors which will be mixed
            for _ in 0..scene.nb_samples {
                let ray = Ray::prime(x, y, aspect_ratio, fov_adjustment, scene);
                color = color + scene.cast_ray(&ray, 0);
            }
            let color = (color * (1. / (scene.nb_samples as f64))).to_rgba();
            let pixel = ((y * scene.width + x) * 3) as usize;

            for i in 0 .. 3 {
                image[pixel + i] = color[i];
            }
        }
    }

    println!("\rCompleted");
    image
}

pub fn render_par(scene: &Scene) -> Vec<u8> {
    let mut image : Vec<u8> = vec![0; (scene.height * scene.width * 3) as usize];
    {
        let mutex = Mutex::new(&mut image);
        let progress = Mutex::new(0);
        let aspect_ratio = (scene.width as f64) / (scene.height as f64);
        let fov_adjustment = (scene.fov.to_radians() / 2.).tan();
        
        (0..scene.height).into_par_iter()
            .for_each(|y| {
                // Process in a buffer
                let mut buf : Vec<u8> = vec![0; 3 * scene.width as usize];
                
                for x in 0..scene.width {
                    let mut color = BLACK;
                    // Get samples of colors which will be mixed
                    for _ in 0..scene.nb_samples {
                        let ray = Ray::prime(x, y, aspect_ratio, fov_adjustment, scene);
                        color = color + scene.cast_ray(&ray, 0);
                    }
                    let color = (color * (1. / (scene.nb_samples as f64))).to_rgba();
                    for i in 0..3 {
                        buf[x as usize * 3 + i] = color[i];
                    }
                }

                // Lock only after having processed in the buffer
                let mut img = mutex.lock().unwrap();
                let mut offset = (y * scene.width * 3) as usize;
                for data in buf.iter() {
                    (*img)[offset] = *data;
                    offset += 1
                }

                let mut p = progress.lock().unwrap();
                *p += 1;
                print!("\r{:02}%", (*p as f32/scene.height as f32 * 100.) as u32);
            });
    }
    image
}

impl Ray {
    /// Generate prime ray with jittering for anti-aliasing.
    pub fn prime(x: u32, y: u32, ratio: f64, fov: f64, scene: &Scene) -> Ray {
        let u1: f64 = thread_rng().gen(); // [0,1)
        let u2: f64 = thread_rng().gen(); // [0,1)

        let sensor_x = (((x as f64 + u1) / scene.width as f64) * 2.0 - 1.0)
                        * ratio * fov;
        let sensor_y = 1.0 - ((y as f64 + u2) / scene.height as f64) * 2.0
                        * fov;
        Ray {
            origin: Vector3 { x: 0., y: 0., z: 0. },
            direction: Vector3 {
                x: sensor_x,
                y: sensor_y,
                z: -1.0,
            }.normalize(),
        }
    }

    pub fn reflect(normal: Vector3, incident: Vector3) -> Vector3 {
        incident - (2.0 * incident.dot(&normal)) * normal
    }

    pub fn refract(normal: Vector3, incident: Vector3, ior: f64) -> Vector3 {
        let mut cosi = normal.dot(&incident);
        let (eta, norm) =
            if cosi < 0. {
                // Outside the surface, cos(theta) should be positive.
                cosi = -cosi;
                (1. / ior, normal)
            } else {
                // Inside the surface, cos(theta) is positive, but reverse the
                // normal direction.
                (ior, -normal)
            };
        let k = 1. - eta * eta * (1. - cosi * cosi);
        if k < 0. {
            // Under the critical angle, total internal reflection.
            Vector3::zero()
        } else {
            eta * incident + (eta * cosi - k.sqrt()) * norm
        }
    }

    /// Compute ratio of reflected light using fresnel equations.
    pub fn fresnel(normal: Vector3, incident: Vector3, ior: f64) -> f64 {
        let mut cosi = normal.dot(&incident);
        let (etai, etat) =
            if cosi < 0. {
                cosi = -cosi;
                (1., ior)
            } else {
                (ior, 1.)
            };

        // Compute sini using Snell's law
        let sint = etai / etat * (1. - cosi * cosi).sqrt();
        // Total internal reflection
        if sint >= 1. {
            1.
        } else {
            let cost = (1. - sint * sint).sqrt();
            let rs = ((etat * cosi) - (etai * cost)) / ((etat * cosi) + (etai * cost));
            let rp = ((etai * cosi) - (etat * cost)) / ((etai * cosi) + (etat * cost));
            (rs * rs + rp * rp) / 2.
            // As a consequence of the conservation of energy,
            // transmittance is given by: kt = 1 - kr;
        }
    }
}

impl<'a> Scene<'a> {
    pub fn trace_max(&self, ray: &Ray, max_distance: f64) -> Option<(&'a Element, f64, Vector3)> {
        let mut index : isize = -1;
        let mut distance = max_distance;
        let mut normal = Vector3::zero();
        for i in 0..self.elements.len() {
            if let Some((d,n)) = self.elements[i].geometry.intersect(&ray) {
                if d < distance {
                    index = i as isize;
                    distance = d;
                    normal = n;
                }
            }
        }
        if index == -1 { None } else {
            Some((&self.elements[index as usize], distance, normal)) }
    }

    pub fn trace(&self, ray: &Ray) -> Option<(&'a Element, f64, Vector3)> {
        self.trace_max(ray, f64::MAX)
    }

    pub fn shade_diffuse(&self, hit_point: Vector3, elt: &Element, normal: Vector3, depth: u32) -> Color {
        
        // Compute the contribution of direct light sources.
        let mut light_color = BLACK;

        for light in &self.lights { match light.typ {
            LightType::Directed(direction) => {
                let dir_to_light = -direction;
                let shadow_ray = Ray {
                    origin: hit_point + (normal * self.bias),
                    direction: dir_to_light,
                };
                if self.trace(&shadow_ray).is_none() {
                    let light_power =
                        normal.dot(&dir_to_light).max(0.0)
                        * light.intensity;
                    light_color = light_color + light.color * light_power;
                }
            },
            LightType::Spherical(origin) => {
                let dir_to_light = (origin - hit_point).normalize();
                let shadow_ray = Ray {
                    origin: hit_point + (normal * self.bias),
                    direction: dir_to_light,
                };
                let dist2_to_light = (origin - hit_point).norm();
                if self.trace_max(&shadow_ray, dist2_to_light.sqrt()).is_none() {
                    let light_power =
                        normal.dot(&dir_to_light).max(0.0)
                         * light.intensity / (4.0 * PI * dist2_to_light);
                    light_color = light_color + light.color * light_power;
                }
            }
        }}

        // Compute the contribution from ambient occlusion.
        let (nb, nt) = normal.coord_system();
        let mut ambient_color = BLACK;

        for n in 0..self.nb_diffuse_rays {
            let u1: f64 = thread_rng().gen(); // [0,1)
            let u2: f64 = thread_rng().gen(); // [0,1)
            let v = ::sample::hemisphere::cosine(u1, u2);
            let diffuse_ray = Ray {
                origin: hit_point + (normal * self.bias),
                direction: v.x * nb + v.y * nt + v.z * normal,
            };
            ambient_color = ambient_color + self.cast_ray(&diffuse_ray, depth + 1);
        }
        // Cosine weighted sampling on the sphere =>
        // L(w) = (Pi / N) * S(N) [f(wi,w) Li(wi)]
        ambient_color = ambient_color * (PI / (self.nb_diffuse_rays as f64));

        // Final diffuse color.
        (elt.material.color * (light_color + ambient_color) * (elt.material.albedo / PI)).clamp()
    }

    pub fn get_color(&self, ray: &Ray, elt: &Element, distance: f64, normal: Vector3, depth: u32) -> Color {

        use scene::SurfaceType::*;

        let hit_point = ray.origin + (ray.direction * distance);

        match elt.material.typ {
            // Area light.
            Emissive => elt.material.color,
            // Reflective surface, mix diffuse light with reflected light.
            Reflect(r) => {
                let reflect_dir = Ray::reflect(normal, ray.direction);
                let reflect_ray = Ray {
                    origin: hit_point + normal * self.bias,
                    direction: reflect_dir
                };
                let diffuse_color = self.shade_diffuse(hit_point, elt, normal, depth);
                let reflect_color = self.cast_ray(&reflect_ray, depth + 1);
                diffuse_color * (1. - r) + reflect_color * r
            },
            // Transparent object, with reflection and refraction.
            ReflectAndRefract(ior) => {
                let mut refract_color = BLACK;
                // Compute fresnel.
                let kr = Ray::fresnel(normal, ray.direction, ior);
                let outside = ray.direction.dot(&normal) < 0.;
                let bias = self.bias * normal;

                // Compute refraction if it is not a case of total
                // internal reflection.
                if kr < 1. {
                    let refract_dir = Ray::refract(normal, ray.direction, ior).normalize();
                    let refract_ori = if outside { hit_point - bias } else { hit_point + bias };
                    let refract_ray = Ray { origin: refract_ori, direction: refract_dir };
                    refract_color = self.cast_ray(&refract_ray, depth + 1);
                }

                // Compute reflection.
                let reflect_dir = Ray::reflect(normal, ray.direction).normalize();
                let reflect_ori = if outside { hit_point + bias } else { hit_point - bias };
                let reflect_ray = Ray { origin: reflect_ori, direction: reflect_dir };
                let reflect_color = self.cast_ray(&reflect_ray, depth + 1);

                // Mix the colors from reflected and refracted light.
                // diffuse_color + reflect_color * kr + refract_color * (1. - kr)
                reflect_color * kr + refract_color * (1. - kr)
            },
            // Diffuse surface.
            _ => self.shade_diffuse(hit_point, elt, normal, depth)
        }
    }

    pub fn cast_ray(&self, ray: &Ray, depth: u32) -> Color {
        if depth >= self.max_rebound {
            BLACK
        } else if let Some((elt, d, n)) = self.trace(ray) {
            self.get_color(ray, elt, d, n, depth)
        } else {
            self.background_color
        }
    }
}
