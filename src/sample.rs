
use vector::Vector3;
use rand::prelude::*;

pub mod sphere {

    use vector::Vector3;
    use std::f64::consts::PI;
    use std::f64;

    // @param u1,u2     random in [0, 1)
    pub fn uniform(u1: f64, u2: f64) -> Vector3 {
        let u3 = 1. - 2. * u1;
        let r = (1. - u3 * u3).max(0.).sqrt();
        let theta = 2. * PI * u2;
        Vector3 {
            x: r * theta.cos(),
            y: r * theta.sin(),
            z: u3,
        }
    }

}

pub mod hemisphere {

    use vector::Vector3;
    use std::f64::consts::PI;
    use std::f64;

    // Cosine weighted random on unit hemisphere.
    // @param u1,u2     random in [0, 1)
    pub fn cosine(u1: f64, u2: f64) -> Vector3 {
        let r = u1.sqrt();
        let theta = 2. * PI * u2;
        Vector3 {
            x: r * theta.cos(),
            y: r * theta.sin(),
            z: (1. - u1).max(0.).sqrt(),
        }
    }

    // @param u1,u2     random in [0, 1)
    pub fn uniform(u1: f64, u2: f64) -> Vector3 {
        let u3 = 1. - 2. * u1;
        let r = (1. - u3 * u3).max(0.).sqrt();
        let theta = 2. * PI * u2;
        Vector3 {
            x: r * theta.cos(),
            y: r * theta.sin(),
            z: u3.abs(),
        }
    }
}

pub fn test(
    nx: Vector3, ny: Vector3, nz: Vector3,
    random: fn(f64, f64) -> Vector3, samples: usize) -> Vec<u8> {
    let mut rng = thread_rng();
    let mut image : Vec<u8> = vec![0; 128 * 128 * 3 * 3];

    fn pixel(img: &mut Vec<u8>, x: usize, y: usize, r: u8, g: u8, b: u8) {
        let w = 3 * 3 * 128;
        img[w * y + 3 * x + 0] = r;
        img[w * y + 3 * x + 1] = g;
        img[w * y + 3 * x + 2] = b;
    };

    fn draw(img: &mut Vec<u8>, v: Vector3, r: u8, g: u8, b: u8) {
        let x = ((v.x * 64. + 64.) as usize).max(0).min(127);
        let y = ((v.y * 64. + 64.) as usize).max(0).min(127);
        let z = ((v.z * 64. + 64.) as usize).max(0).min(127);

        pixel(img, x, y, r, g, b);
        pixel(img, x + 128, z, r, g, b);
        pixel(img, y + 256, z, r, g, b);
    };

    for s in 0..samples {
        let s = random(rng.gen_range(-1., 1.), rng.gen_range(-1., 1.));
        let v = s.x * nx + s.y * ny + s.z * nz;

        draw(&mut image, v, 255, 255, 255);
    }

    draw(&mut image, nz, 255, 0, 0);
    draw(&mut image, -1. * nz, 0, 255, 0);
    image
}
