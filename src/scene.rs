
use vector::Vector3;
use color::Color;
use geometry::Geometry;

#[derive(Copy, Clone)]
pub enum SurfaceType {
    Diffuse,
    Emissive,
    Reflect(f64),
    ReflectAndRefract(f64),
}

pub struct Material {
    pub color: Color,
    pub albedo: f64,
    pub typ: SurfaceType,
}

pub struct Element<'a> {
    pub geometry: &'a Geometry,
    pub material: Material,
}

pub enum LightType {
    Directed(Vector3),
    Spherical(Vector3),
}

pub struct Light {
    pub typ: LightType,
    pub color: Color,
    pub intensity: f64,
}

pub struct Scene<'a> {
    pub width:  u32,
    pub height: u32,
    pub fov:    f64,
    pub elements: Vec<Element<'a>>,
    pub lights: Vec<Light>,
    pub background_color: Color,
    pub bias: f64,
    pub max_rebound: u32,
    pub nb_samples: usize,
    pub nb_diffuse_rays: usize,
}
